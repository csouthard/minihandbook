# minihandbook

## Rationale

This project is intended to be a sandbox for Product Manager in Product Operations technical interviews at GitLab.

The exercises should examine: iteration, working with structured data files, debugging skills, information architecture, automation, Ruby familiarity, git skills, and collaboration skills. The assignments should directly reflect the role will as daily tasks will require an ability to fluidly change the handbook and an ability to manipulate / automate data into easily digestible forms (handbook entries, etc).

## Installation

Below are two known working installation options, local and cloud dev environment. Depending on your computer, experience level, and preferences, you can choose either path.

### Local Development Environment

Difficulty: Medium

The following configuration has been developed and tested on Mac OS Big Sur, but _should_ work on other versions of Mac OS, Windows, and Linux, but those have not been tested.

- Ruby 2.7.4 or 3.0.1 (tested locally)
- Node 14.17.5
- a text editor or IDE of your choice (e.g., [VS Code](https://code.visualstudio.com/))

If you use a version manager like [asdf](https://github.com/asdf-vm/asdf), you can run `asdf install` to install the versions listed above provided that you have the [Ruby](https://github.com/asdf-vm/asdf-ruby) and [NodeJS](https://github.com/asdf-vm/asdf-nodejs) plugins and other dependencies on your computer including [XCode command line tools](https://www.freecodecamp.org/news/install-xcode-command-line-tools/).

There are several other version managers use whatever is comfortable and familiar.  There are strong dependencies on Ruby and Node libraries required to make this project run correctly.

The GitLab handbook has a [useful section on getting setup](https://about.gitlab.com/handbook/git-page-update/) to edit _the handbook_ locally, those steps can apply here with the difference being that you should clone this project and not _the handbook_ repository.

#### Steps after preliminary setup

1. Install the latest [Ruby](https://www.ruby-lang.org/en/) version (3.0) and the LTS version of [NodeJS](https://nodejs.org/en/) if you have not done so already. See comments about version managers and setup help above.
2. change directory, `cd`, to the location where you cloned this repository, then
3. Install the required libraries.
  - run `bundle install` for Ruby libraries
  - run `npm install` for NodeJS libraries
4. run `bundle exec middleman serve`
5. visit http://localhost:4567 your browser

### Cloud Development Environment via Gitpod

Difficulty: Easy-ish

A [configuration file](/-/blob/main/.gitpod.yml) is provided for developing this project on [Gitpod](https://www.gitpod.io/).

1. In your browser’s address bar, prefix the entire project URL with `gitpod.io/#` and press Enter.
  - For example,` gitpod.io/#https://gitlab.com/csouthard/minihandbook`
2. Sign in with your GitLab credentials (you will likely have to allow Gitpod permission) and let the workspace start up. The interface will be reminiscent of VS Code. A terminal window at the bottom should be active installing dependencies, but if it doesn't automatically you can use steps 3 and 4 above in the terminal window to install dependencies and start the server.

From here, you can make changes and test the site out in the preview window or in a new tab.

#### Committing Changes on Gitpod

Committing changes from Gitpod will work but you might need allow further permissions on your GitLab to enable that behavior should that be the case you will be prompted.

You should be able to branch and make changes in a terminal window or in the source control panel on the left. [These instructions](https://code.visualstudio.com/docs/editor/versioncontrol) for that should be helpful if that interface is confusing.

### Alternative Installations

#### Docker

A docker configuration is not available yet. Suggestion: Use Gitpod instead (see above).

#### Just use GitLab's editing tools

This is an option for branching and making changes, but we do not have a [review apps](https://docs.gitlab.com/ee/ci/review_apps/) configured for this project so you won't be able to access a working preview. Suggestion: Use Gitpod or local development instead (see above).

## Your Assignment

Work through the following issues using a git branching strategy and merge requests. Should you get stuck, you're free to google for answers or other references for inspiration. Should you get really stuck, write down your thoughts, struggles, questions in the issues linked below. If your familiarity with the Ruby programming language is a major blocker, you could propose solutions in a more familiar language, or pseudocode. During the interview we'll discuss each of the issues and your contributions to solving them. Don't worry if you cannot complete all of the issues with full working solution.

- #1 - on the home page, under `Resources` to be a more useful HTML structure
- #2 - on the home page, list secret projects alphabetically
- #3 - on `project.md.erb`, change `secret_projects` to `super_secret_projects` and resort in reverse order (high to low) by `milestone_release`
- #4 - on `/products`, update the view such that the product name, rank, and description are displayed in a structured manner
- #5 - SPIKE on `/analytics/maturity.html`, how could we update the quarterly headers automatically
- #6 - BUG on `/issues`, debug why this page doesn't render properly

### Resources

- [git reference](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
- [gitlab issues](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html)
- [gitlab merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html)
- [middleman docs](https://middlemanapp.com/basics/install/)
- [ruby core docs](https://ruby-doc.org/core-2.7.0/)
- [Gitpod documentation](https://www.gitpod.io/docs/workspaces)
