set :markdown_engine, :kramdown

# Don't render or include the following into the sitemap
ignore '**/.gitkeep'

# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

activate :autoprefixer do |prefix|
  prefix.browsers = "last 2 versions"
end
activate :livereload
activate :external_pipeline,
          name: :webpack,
          command: build? ? './node_modules/webpack/bin/webpack.js --bail' : './node_modules/webpack/bin/webpack.js --watch -d eval --color',
          source: ".tmp/dist",
          latency: 1

# Layouts
# https://middlemanapp.com/basics/layouts/

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# With alternative layout
# page '/path/to/file.html', layout: 'other_layout'

# Proxy pages
# https://middlemanapp.com/advanced/dynamic-pages/

# proxy(
#   '/this-page-has-no-template.html',
#   '/template-file.html',
#   locals: {
#     which_fake_page: 'Rendering a fake page with a local variable'
#   },
# )

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/

# helpers do
#   def some_helper
#     'Helping'
#   end
# end

helpers do
  require "addressable/uri"
  require "httparty"
  require "json"

  API_KEY = ENV.fetch("GITLAB_API_KEY", "")
  BASE_HEADERS = {
    "PRIVATE-TOKEN"  => API_KEY,
    "Content-Type" => "application/json"
  }

  def get_reqeust(endpoint)
    base = "https://gitlab.com/"
    uri = Addressable::URI.escape("/api/v4/#{endpoint}")
    url = Addressable::URI.join(base, uri)

    puts url.to_s

    results = HTTParty.get(url.to_s,
      follow_redirects: true,
      maintain_method_across_redirects: true,
      headers: BASE_HEADERS)

    if results.success?
      results
    else
      raise StandardError
    end
  end

  def get_labeled_issues(group_id=9970, state="closed", *labels)
    puts group_id
    endpoint = "groups/#{group_id}/issues?scope=all&state=#{state}"
    puts endpoint

    if labels.any?
      endpoint += "&labels=#{labels.join(",")}"
    end

    puts endpoint
    JSON.parse(get_reqeust(endpoint).response)
  end

  def secret_projects
    %w(aardvark otter walrus ant bat)
  end

  Project = Struct.new(:name, :milestone_release, :description) do
    def to_s
      name
    end
  end

  def super_secret_projects
    ssp1 = Project.new("Proud Lightfoot", 42.0, "a nearly silent propulsion system")
    ssp2 = Project.new("Idle Watchdog", 42.24, "robot that checks repos for secrets")
    ssp3 = Project.new("Awkward Shadow", 1.0, "admin can impersonate a user")
    ssp4 = Project.new("Pretty Devil", 33.3, "cool new swag hoodie")

    [ssp1, ssp2, ssp3, ssp4]
  end
end

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings

# configure :build do
#   activate :minify_css
#   activate :minify_javascript
# end

configure :development do
  config[:css_dir] = ".tmp/dist"
  config[:js_dir] = ".tmp/dist"
end


